const items=require('./items')

function map(items,cb){
    if(items==undefined||cb==undefined){
        return []
    }
    let arrAccordingTocb=[]
    for(let item of items){
        arrAccordingTocb.push(cb(item))
    }
    return arrAccordingTocb   
}

module.exports=map