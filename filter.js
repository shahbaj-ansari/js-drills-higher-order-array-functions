

function filter(elements,cb){
    if(elements==undefined||cb==undefined){
        return []
    }
    let filteredArray=[]
    for(let item of elements){
        if(cb(item)==true){
            filteredArray.push(item)
        }
    }
    return filteredArray
}

module.exports=filter