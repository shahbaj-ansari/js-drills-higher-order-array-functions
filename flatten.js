
function reduceNesting(array){
    if(typeof(array)=="number"){
        return array;
    }else{
        return reduceNesting(array[0])
    }
}

function flatten(elements){
    if(flatten==undefined){
        return []
    }
    let flattenedArray=[]
    for(let i=0;i<elements.length;i++){
        flattenedArray.push(reduceNesting(elements[i]));
    }
    return flattenedArray
}

/*  Iterative Solution 

function flatten(elements){
    if(flatten==undefined){
        return []
    }
    let flattenedArray=[]
    for(let i=0;i<elements.length;i++){
        let currIdx=elements[i]
        while(1){
            if(typeof(currIdx)=="number"){
                flattenedArray.push(currIdx);
                break;
            }
            currIdx=currIdx[0]
        }
    }
    return flattenedArray
}
*/

module.exports=flatten