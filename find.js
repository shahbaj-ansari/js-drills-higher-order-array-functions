
function find(elements,cb){
    if(elements==undefined||cb==undefined){
        return []
    }
    for(let item of elements){
        if(cb(item)==true){
            return item;
        }
    }
    return []   
}

module.exports=find