const items=require('./items')

function each(element,cb){
    if(element==undefined||cb==undefined){
        return []
    }
    for(let val of element){
        cb(val);
    }
}

module.exports=each

