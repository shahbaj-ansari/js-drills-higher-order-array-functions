

function reduce(elements,cb,startingValue){
    if(elements==undefined||cb==undefined){
        return []
    }
    for(let item of elements){
        if(startingValue==undefined){
            startingValue=elements[0];
            continue;
        }
        startingValue=cb(startingValue,item)
    }
    return startingValue
}

module.exports=reduce